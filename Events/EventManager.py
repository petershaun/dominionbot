from enum import Enum


class EventTypes(Enum):
    JOIN_EVENT = 0


class EventManager(object):

    def __init__(self):
        self.handlers = list()

    def register_event_handler(self, handler):
        self.handlers.append(handler)

    def run_event(self, type, **kwargs):
        for handler in self.handlers:
            if handler.type == type:
                handler.run()
