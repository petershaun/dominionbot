from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from functools import wraps
import logging
import Game
from Player import Player
import DeckBuilder
import key
import GUIManager
import Events.EventManager as EventManager


updater = Updater(token=key.KEY)
dispatcher = updater.dispatcher
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
games = dict()
tg_id_game = dict()
LIST_OF_ADMINS = [62619149]
deck_builder = None
gui_manager = None
event_manager = None

def restricted(func):
    @wraps(func)
    def wrapped(bot, update, *args, **kwargs):
        user_id = update.effective_user.id
        if user_id not in LIST_OF_ADMINS:
            bot.send_message(chat_id=update.effective_user.id, text="Das kann ich leider nicht zulassen, {}.".format(update.effective_user.username))
            print('Access denied for {}'.format(user_id))
            return
        return func(bot, update, *args, **kwargs)
    return wrapped


def start(bot, update):
    bot.send_message(chat_id=update.message.chat_id, text='Hallo! Ich bin der Dominion bot. Um ein Spiel zu starten, füge mich in eine Gruppe hinzu und führe /creategame aus!')


def create_game(bot, update):
    chat_id = update.message.chat_id
    creator = Player(id=0, draw_stack=Game.create_starting_stack(deck_builder), tg_user_id=update.effective_user.id)
    purchasable_stacks = list()
    purchasable_stacks.append(deck_builder.get_copy('Dorf', 12))
    purchasable_stacks.append(deck_builder.get_copy('Anwesen', 12))
    purchasable_stacks.append(deck_builder.get_copy('Kupfer', 12))
    purchasable_stacks.append(deck_builder.get_copy('Silber', 12))
    purchasable_stacks.append(deck_builder.get_copy('Gold', 12))
    purchasable_stacks.append(deck_builder.get_copy('Herzogtum', 12))
    purchasable_stacks.append(deck_builder.get_copy('Provinz', 12))
    id = len(games)
    game = Game.Game(players=[creator], id=id, buyable_card_stacks=purchasable_stacks)
    games[chat_id] = game
    tg_id_game[update.effective_user.id] = game
    bot.send_message(chat_id=chat_id, text='Spiel mit der ID {0} erstellt! Spieler in dieser Gruppe können jetzt mit /join beitreten!'.format(id))


def join_game(bot, update):
    chat_id = update.message.chat_id
    if chat_id in games:
        if tg_id_game[update.effective_user.id] is not None:
            bot.send_message(chat_id=update.effective_user.id, text='Bereits in einem Spiel!')
        else:
            game = games[chat_id]
            new_player = Player(id=len(game.players), draw_stack=Game.create_starting_stack(deck_builder), tg_user_id=update.effective_user.id)
            game.players.append(new_player)
            bot.send_message(chat_id=chat_id, text='{0} ist dem Spiel beigetreten!'.format(update.effective_user.username))
            bot.send_message(chat_id=new_player.tg_user_id, text='Willkommen im Spiel mit der ID {0}'.format(game.id))
    else:
        bot.send_message(chat_id=update.effective_user.id, text='Ich wüsste nicht, welchem Spiel du beitreten willst...')


def cancel_command(bot, update):
    tg_id = update.effective_user.id
    if not gui_manager.get_handler(tg_id) is None:
        if gui_manager.get_handler(tg_id)[1]:
            gui_manager.set_hanfler(tg_id, None)
            bot.send_message(chat_id=tg_id, text='Vorgang abgebrochen.')
        else:
            bot.send_message(chat_id=tg_id, text='Vorgang ist nicht abbrechbar')
    else:
        bot.send_message(chat_id=tg_id, text='Es ist momentan nicht im Gange.')


def dev_command_handler(bot, update):
    command = update.message.text
    code = compile(command[command.find(" ") + 1:], '<string>', command.split(' ')[0])
    try:
        result = eval(code)
    except BaseException as e:
        result = 'Das ging schief 🙄'
        print(e)
    bot.send_message(chat_id=update.effective_user.id, text=str(result))


@restricted
def dev_mode(bot, update):
    gui_manager.set_handler(update.effective_user.id, dev_command_handler)
    bot.send_message(chat_id=update.effective_user.id, text='Dev Mode aktiviert.')


def userid_teller(bot, update):
    bot.send_message(chat_id=update.effective_user.id, text=str(update.effective_user.id))


def start_bot():
    global deck_builder
    deck_builder = DeckBuilder.DeckBuilder()
    global gui_manager
    gui_manager = GUIManager.GUIManager()
    global event_manager
    event_manager = EventManager.EventManager()
    start_handler = CommandHandler('start', start)
    create_game_handler = CommandHandler('creategame', create_game)
    join_game_handler = CommandHandler('join', join_game)
    cancel_command_handler = CommandHandler('cancel', cancel_command)
    dev_command_handler = CommandHandler('dev', dev_mode)
    userid_teller_handler = CommandHandler('getid', userid_teller)
    dispatcher.add_handler(start_handler)
    dispatcher.add_handler(create_game_handler)
    dispatcher.add_handler(join_game_handler)
    dispatcher.add_handler(cancel_command_handler)
    dispatcher.add_handler(dev_command_handler)
    dispatcher.add_handler(userid_teller_handler)

    chat_handler = MessageHandler(Filters.text, gui_manager.on_chat)
    dispatcher.add_handler(chat_handler)
    updater.start_polling()


def get_deck_builder():
    return deck_builder


if __name__ == '__main__':
    start_bot()
