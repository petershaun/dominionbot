from enum import Enum


class Phase(Enum):
    DRAW_PHASE = 0
    ACTION_PHASE = 1
    BUY_PHASE = 2
    CLEANUP_PHASE = 3
