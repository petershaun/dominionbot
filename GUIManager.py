class ButtonSet(object):

    def __init__(self, *labels):
        self.labels = labels


class GUIManager(object):

    def __init__(self):
        self.__input_handlers = dict()

    def request_user_action(self, button_set, tg_id, handler):
        pass

    def get_buttonset_by_cardstack(self, cardstack):
        pass

    def set_handler(self, tg_id, handler, cancelable=True):
        self.__input_handlers[tg_id] = (handler, cancelable)

    def get_handler(self, tg_id):
        return self.__input_handlers[tg_id]

    def on_chat(self, bot, update):
        if update.effective_user.id in self.__input_handlers and not self.__input_handlers[update.effective_user.id] is None:
            handler = self.__input_handlers[update.effective_user.id][0]
            handler(bot, update)
        else:
            bot.send_message(chat_id=update.effective_user.id, text='Das verstehe ich jetzt nicht 🤔')

