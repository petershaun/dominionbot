from Phase import Phase
from Cards.CardStack import CardStack


class Game(object):

    current_turn = 0
    triggers = {}
    on_turn = None
    current_phase = -1
    buyable_card_stacks = []
    trash_stack = CardStack()

    def __init__(self, players, id, buyable_card_stacks):
        self.players = players
        self.id = id
        self.buyable_card_stacks = buyable_card_stacks
        for i in range(len(players)):
            players[i].id = i

    def process_triggers(self):
        for trigger in self.triggers:
            if trigger.player.id == self.on_turn.id and trigger.phase == self.current_phase:
                trigger.effect(player=self.on_turn)

    def enter_phase(self, phase):
        self.current_phase = phase
        self.process_triggers()
        if phase == Phase.DRAW_PHASE:
            self.on_turn.draw(amount=5)
        elif phase == Phase.ACTION_PHASE:
            pass
        elif phase == Phase.BUY_PHASE:
            pass
        elif phase == Phase.CLEANUP_PHASE:
            used = list()
            used.append(self.on_turn.hand_stack.draw_cards(amount=len(self.on_turn.hand_stack)))
            used.append(self.on_turn.action_stack.draw_cards(amount=len(self.on_turn.action_stack)))
            self.on_turn.discard_stack.append(used)
            self.on_turn.actions = 1
            self.on_turn.buys = 1
            self.on_turn.money = 0

    def next_turn(self):
        next_player = (self.on_turn.id + 1) % len(self.players)
        self.on_turn = self.players[next_player]
        self.enter_phase(Phase.DRAW_PHASE)
        self.next_turn()

    def get_player_by_tg_id(self, tg_id):
        for player in self.players:
            if player.tg_user_id == tg_id:
                return player


def create_starting_stack(deck_builder):
    stack = CardStack()
    stack.cards.append(deck_builder.get_copy('Kupfer', 5).cards)
    stack.cards.append(deck_builder.get_copy('Anwesen', 5).cards)
    return stack

