from Cards.TreasureCard import TreasureCard
from Cards.VictoryCard import VictoryCard
from Cards import ActionCard
from SpecialCards.GardenCard import GardenCard
from copy import copy
from Cards.CardStack import CardStack


class DeckBuilder(object):
    known_cards = {}

    def __init__(self):
        self.build_cards()

    def register_card(self, card):
        self.known_cards[card.name] = card

    def build_cards(self):
        # treasure cards
        copper = TreasureCard(name='Kupfer', value=1, cost=0, description='Gewährt diese Runde +1 Geld')
        self.register_card(copper)
        silver = TreasureCard(name='Silber', value=2, cost=3, description='Gewährt diese Runde +2 Geld')
        self.register_card(silver)
        gold = TreasureCard(name='Gold', value=3, cost=6, description='Gewährt diese Runde +3 Geld')
        self.register_card(gold)
        # victory cards
        estate = VictoryCard(name='Anwesen', value=1, cost=2, description='Gewährt 1 Siegpunkt')
        self.register_card(estate)
        duchy = VictoryCard(name='Herzogtum', value=3, cost=5, description='Gewährt 3 Siegpunkte')
        self.register_card(duchy)
        province = VictoryCard(name='Provinz', value=6, cost=8, description='Gewährt 6 Siegpunkte')
        self.register_card(province)
        gardens = GardenCard(name='Gärten', cost=4, description='Wert 1 Siegpunkt für je 10 Karten (abgerundet) im eigenen Kartensatz bei Spielende.')
        self.register_card(gardens)
        # action cards
        cellar = ActionCard.ActionCard(name='Keller', effect=ActionCard.cellar_effect, cost=2, description='+1 Aktion. Lege eine beliebige Anzahl Karten aus deiner Hand ab. Für jede abgelegte Karte ziehst du eine Karte.')
        self.register_card(cellar)
        chapel = ActionCard.ActionCard(name='Kapelle', effect=ActionCard.chapel_effect, cost=2, description='Entsorge bis zu 4 Karten aus deiner Hand.')
        self.register_card(chapel)
        harbinger = ActionCard.ActionCard(name='Vorbotin', effect=ActionCard.harbinger_effect, cost=3, description='+1 Karte. +1 Aktion. Sieh deinen Ablagestapel durch. Du darfst eine Karte daraus auf deinen Nachziehstapel legen.')
        self.register_card(harbinger)
        merchant = ActionCard.ActionCard(name='Händerlin', effect=ActionCard.merchant_effect, cost=3, description='+1 Karte. +1 Aktion. Spielst du in diesem Zug das erste Mal Silber aus: +1 Geld')
        self.register_card(merchant)
        vassal = ActionCard.ActionCard(name='Vasall', effect=ActionCard.vassal_effect, cost=3, description='+2 Geld. Lege die oberste Karte deines Nachziehstapels ab. Ist es eine Aktionskarte, darfst du sie ausspielen.')
        self.register_card(vassal)
        village = ActionCard.ActionCard(name='Dorf', effect=ActionCard.village_effect, cost=3, description='+1 Karte. +2 Aktionen')
        self.register_card(village)
        workshop = ActionCard.ActionCard(name='Werkstatt', effect=ActionCard.workshop_effect, cost=3, description='Nimm dir eine Karte, die bis zu 4 Geld kostet.')
        self.register_card(workshop)
        moneylender = ActionCard.ActionCard(name='Geldverleiher', effect=ActionCard.moneylender_effect, cost=4, description='Du darfst ein Kupfer aus der Hand entsorgen. Wenn du das tust: +3 Geld')
        self.register_card(moneylender)
        poacher = ActionCard.ActionCard(name='Wilddiebin', effect=ActionCard.poacher_effect, cost=4, description='+1 Karte. +1 Aktion. +1 Geld. Lege pro leerem Vorratsstapel eine Handkarte ab.')
        self.register_card(poacher)
        remodel = ActionCard.ActionCard(name='Umbau', effect=ActionCard.remodel_effect, cost=4, description='Entsorge eine Karte aus deiner Hand. Nimm eine Karte, die bis zu 2 Geld mehr kostet als die entsorgte Karte.')
        self.register_card(remodel)
        smithy = ActionCard.ActionCard(name='Schmiede', effect=ActionCard.smithy_effect, cost=4, description='+3 Karten')
        self.register_card(smithy)
        throne_room = ActionCard.ActionCard(name='Thronsaal', effect=ActionCard.throne_room_effect, cost=4, description='Du darfst eine Aktionskarte aus der Hand zweimal ausspielen.')
        self.register_card(throne_room)
        council_room = ActionCard.ActionCard(name='Ratsversammlung', effect=ActionCard.council_room_effect, cost=5, description='+4 Karten. +1 Kauf. Jeder Mitspieler zieht eine Karte')
        self.register_card(council_room)
        festival = ActionCard.ActionCard(name='Jahrmarkt', effect=ActionCard.festival_effect, cost=5, description='+2 Aktionen. +1 Kauf. +2 Geld.')
        self.register_card(festival)
        laboratory = ActionCard.ActionCard(name='Laboratorium', effect=ActionCard.laboratory_effect, cost=5, description='+2 Karten. +1 Aktion')
        self.register_card(laboratory)
        library = ActionCard.ActionCard(name='Bibliothek', effect=ActionCard.library_effect, cost=5, description='Zieh, bis du 7 Karten auf der Hand hast; gezogene Aktionskarten darfst du dabei zur Seite legen und nach dem Ziehen ablegen.')
        self.register_card(library)
        market = ActionCard.ActionCard(name='Markt', effect=ActionCard.market_effect, cost=5, description='+1 Karte. +1 Aktion. +1 Kauf. +1 Geld.')
        self.register_card(market)
        mine = ActionCard.ActionCard(name='Mine', effect=ActionCard.mine_effect, cost=5, description='Du darfst eine Geldkarte aus der Hand entsorgen. Nimm eine Geldkarte auf die Hand, die bis zu 3 Geld mehr kostet.')
        self.register_card(mine)
        sentry = ActionCard.ActionCard(name='Torwächterin', effect=ActionCard.sentry_effect, cost=5, description='+1 Karte. +1 Aktion. Sieh dir die obersten 2 Karten deines Nachziehstapels an. '
                                                                                                                 'Entsorge und/oder lege beliebig viele davon ab. Lege die übrigen Karten in beliebiger Reihenfolge auf deinen Nachziehstapel zurück.')
        self.register_card(sentry)
        artisan = ActionCard.ActionCard(name='Töpferei', effect=ActionCard.artisan_effect, cost=6, description='Nimm eine Karte vom Vorrat auf die Hand, die bis zu 5 Geld kostet. Lege eine Handkarte auf deinen Nachziehstapel.')
        self.register_card(artisan)
        chancellor = ActionCard.ActionCard(name='Kanzler', effect=ActionCard.chancellor_effect, cost=3, description='+2 Geld. Du darfst sofort deinen kompletten Nachziehstapel auf deinen Ablagestapel legen.')
        self.register_card(chancellor)
        woodcutter = ActionCard.ActionCard(name='Holzfäller', effect=ActionCard.woodcutter_effect, cost=3, description='+1 Kauf. +2 Geld.')
        self.register_card(woodcutter)
        feast = ActionCard.ActionCard(name='Festmahl', effect=ActionCard.feast_effect, cost=4, description='Entsorge diese Karte. Nimm dafür eine Karte vom Vorrat, die höchstens 5 Geld kostet.')
        self.register_card(feast)
        adventurer = ActionCard.ActionCard(name='Abenteurer', effect=ActionCard.adventurer_effect, cost=6, description='Decke solange Karten vom Nachziehstapel auf, bis du 2 Geldkarten aufgedeckt hast. Nimm diese auf die Hand und lege die anderen aufgedeckten Karten ab.')
        self.register_card(adventurer)

    def get_copy(self, name, amount=1):
        if amount == 1:
            return copy(self.known_cards[name])
        else:
            stack = CardStack()
            for i in range(amount):
                stack.cards.append(copy(self.known_cards[name]))
            return stack
