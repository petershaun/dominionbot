from Cards.CardStack import CardStack
from Cards.CardStack import NotEnoughCardsError
from Cards.CardType import CardType


class Player(object):

    hand_stack = CardStack()
    discard_stack = CardStack()
    action_stack = CardStack()

    actions = 1
    buys = 1
    money = 0

    def __init__(self, id, draw_stack, tg_user_id):
        self.id = id
        self.draw_stack = draw_stack
        self.tg_user_id = tg_user_id

    def draw(self, amount):
        for i in range(amount):
            try:
                self.draw_stack.transfer_card(self.hand_stack)
            except NotEnoughCardsError:
                discarded = self.hand_stack.draw_cards(amount=len(self.hand_stack))
                self.draw_stack.add_cards(discarded)
                self.draw_stack.shuffle()
                self.draw_stack.transfer_card(self.hand_stack)

    def get_sorted_hand(self, categories=[CardType.ACTION, CardType.VICTORY, CardType.TREASURE, CardType.CURSE], collapsed=False):
        if collapsed:
            card_count = dict()
            for handcard in self.hand_stack.cards:
                if handcard.get_type() in categories:
                    if handcard in card_count.items():
                        assert isinstance(card_count[handcard], int)
                        card_count[handcard] += 1
                    else:
                        card_count[handcard] = 0
            return sorted(card_count)
        else:
            sorted_hand = list()
            for handcard in self.hand_stack.cards:
                if handcard.get_type() in categories:
                    sorted_hand.append(handcard)
            return sorted(sorted_hand)