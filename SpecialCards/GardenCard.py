from Cards.VictoryCard import VictoryCard
import math


class GardenCard(VictoryCard):

    def __init__(self, name, description, cost):
        super().__init__(self, name, description, cost)

    def get_value(self, player):
        return math.floor((player.draw_stack + player.action_stack + player.discard_stack) / 10)
