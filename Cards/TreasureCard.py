from Cards.Card import Card
from Cards.CardType import CardType


class TreasureCard(Card):

    def __init__(self, name, description, cost, effect=None, value=0):
        self.effect = effect
        self.value = value
        super().__init__(name, cost, description=description)

    def __radd__(self, other):
        return self.value + other

    def __add__(self, other):
        return self.value + other

    def __lt__(self, other):
        if isinstance(other, TreasureCard):
            return self.value < other.value
        elif isinstance(other, Card):
            return self.get_type().value < other.get_type().value
        elif isinstance(other, (float, int)):
            return self.get_type().value < other
        else:
            return NotImplemented

    def get_type(self):
        return CardType.TREASURE

    def run_effect(self, **kwargs):
        self.effect(**kwargs)
