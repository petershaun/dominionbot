from random import shuffle


class CardStack(object):

    def __init__(self, cards=list()):
        self.cards = cards

    def __len__(self):
        return len(self.cards)

    def shuffle(self):
        shuffle(self.cards)

    def add_cards(self, *cards):
        self.cards.append(cards)

    def transfer_card(self, other_stack):
        if len(self.cards) < 1:
            raise NotEnoughCardsError()
        else:
            other_stack.add_card(self.cards.pop(0))

    def draw_cards(self, amount=1):
        drawn = []
        if amount > len(self.cards):
            raise NotEnoughCardsError()
        else:
            for i in range(0, amount):
                drawn.append(self.cards.pop(0))
        return drawn


class NotEnoughCardsError(ValueError):
    pass
