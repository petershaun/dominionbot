from Cards.Card import Card
from Cards.CardType import CardType


class VictoryCard(Card):

    def __init__(self, name, description, cost, value=0):
        self.value = value
        super().__init__(name, description, cost)

    def __radd__(self, other):
        return self.value + other

    def __add__(self, other):
        return self.value + other

    def __lt__(self, other):
        if isinstance(other, VictoryCard):
            return self.value < other.value
        elif isinstance(other, Card):
            return self.get_type().value < other.get_type().value
        elif isinstance(other, (float, int)):
            return self.get_type().value < other
        else:
            return NotImplemented

    def get_type(self):
        return CardType.VICTORY

    def get_value(self, player=None):
        return self.value
