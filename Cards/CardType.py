from enum import Enum


class CardType(Enum):
    NONE = 0
    ACTION = 1
    TREASURE = 2
    VICTORY = 3
    CURSE = 4
    ATTACK = 5
    DURATION = 6
    REACTION = 7
