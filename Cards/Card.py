from Cards import CardType


class Card(object):

    def __init__(self, name, cost, description=''):
        self.name = name
        self.description = description
        self.cost = cost

    def __lt__(self, other):
        if isinstance(other, Card):
            return self.get_type() < other.get_type()
        elif isinstance(other, (float, int)):
            return self.get_type() < other
        else:
            return NotImplemented

    def get_type(self):
        return CardType.NONE

    def get_additional_type(self):
        return CardType.NONE
