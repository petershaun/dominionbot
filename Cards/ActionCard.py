from Cards.Card import Card
from Cards.CardType import CardType


class ActionCard(Card):

    def __init__(self, name, effect, description, cost):
        self.effect = effect
        super().__init__(name, cost, description)

    def __lt__(self, other):
        if isinstance(other, ActionCard):
            return self.cost < other.cost
        elif isinstance(other, Card):
            return self.get_type().value < other.get_type().value
        elif isinstance(other, (float, int)):
            return self.get_type().value < other
        else:
            return NotImplemented

    def get_type(self):
        return CardType.ACTION

    def run_effect(self, **kwargs):
        self.effect(**kwargs)


def cellar_effect(**kwargs):
    kwargs['player'].actions += 1
    pass


def chapel_effect(**kwargs):
    pass


def harbinger_effect(**kwargs):
    kwargs['player'].draw(1)
    kwargs['player'].actions += 1
    pass


def merchant_effect(**kwargs):
    kwargs['player'].draw(1)
    kwargs['player'].actions += 1
    pass


def vassal_effect(**kwargs):
    kwargs['player'].money += 2
    pass


def village_effect(**kwargs):
    kwargs['player'].draw(1)
    kwargs['player'].actions += 2


def workshop_effect(**kwargs):
    pass


def moneylender_effect(**kwargs):
    pass


def poacher_effect(**kwargs):
    kwargs['player'].draw(1)
    kwargs['player'].actions += 1
    kwargs['player'].money += 1
    pass


def remodel_effect(**kwargs):
    pass


def smithy_effect(**kwargs):
    kwargs['player'].draw(3)


def throne_room_effect(**kwargs):
    pass


def council_room_effect(**kwargs):
    kwargs['player'].draw(4)
    kwargs['player'].buys += 1
    for player in kwargs['game'].players:
        if player.id != kwargs['player'].id:
            player.draw(1)


def festival_effect(**kwargs):
    kwargs['player'].actions += 2
    kwargs['player'].buys += 1
    kwargs['player'].money += 2


def laboratory_effect(**kwargs):
    kwargs['player'].draw(2)
    kwargs['player'].actions += 2


def library_effect(**kwargs):
    pass


def market_effect(**kwargs):
    kwargs['player'].draw(1)
    kwargs['player'].actions += 1
    kwargs['player'].buys += 1
    kwargs['player'].money += 1


def mine_effect(**kwargs):
    pass


def sentry_effect(**kwargs):
    kwargs['player'].draw(1)
    kwargs['player'].actions += 1
    pass


def artisan_effect(**kwargs):
    pass


def chancellor_effect(**kwargs):
    kwargs['player'].money += 2
    pass


def woodcutter_effect(**kwargs):
    kwargs['player'].buys += 1
    kwargs['player'].money += 2


def feast_effect(**kwargs):
    pass


def spy_effect(**kwargs):
    kwargs['player'].draw(1)
    kwargs['player'].actions += 1


def thief_effect(**kwargs):
    pass


def adventurer_effect(**kwargs):
    pass
